import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';

import { AppComponent } from './app/Component/app.component';
import { BookListComponent } from './app/Component/book-list.component';
import { BookDetailsComponent } from './app/Component/book-details.component';
import { UserListComponent } from './app/Component/user-list.component';
import { UserProfileComponent } from './app/Component/user-profile.component';
import { LoginComponent } from './app/Component/login.component';
import { RegisterComponent } from './app/Component/register.component';
import { NavbarComponent } from './app/Component/navbar.component';

const routes: Routes = [
  { path: 'books', component: BookListComponent },
  { path: 'books/:id', component: BookDetailsComponent },
  { path: 'users', component: UserListComponent },
  { path: 'profile', component: UserProfileComponent },
  { path: 'login', component: LoginComponent },
  { path: 'register', component: RegisterComponent },
  { path: '', redirectTo: 'login', pathMatch: 'full' }
];

@NgModule({
  declarations: [
    AppComponent,
    BookListComponent,
    BookDetailsComponent,
    UserListComponent,
    UserProfileComponent,
    LoginComponent,
    RegisterComponent,
    NavbarComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    RouterModule.forRoot(routes)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
