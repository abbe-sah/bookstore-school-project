/**
 * Author: Abbe S
 *
 * Denna komponent implementerar navigeringsfältet, checkar av ifall det en user eller admin för att visa menyn.
 * 

*/
import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';

interface DecodedToken {
  role: string;
}

@Component({
  selector: 'app-navbar',
  templateUrl: '../View/navbar.component.html',
  styleUrls: ['../Style/navbar.component.css']
})
export class NavbarComponent implements OnInit {
  isNavbarVisible = false;

  constructor(private router: Router) {}

  ngOnInit(): void {
    this.checkNavbarVisibility();
    this.router.events.subscribe((event) => {
      if (event instanceof NavigationEnd) {
        this.checkNavbarVisibility();
      }
    });
  }

  checkNavbarVisibility(): void {
    const accessToken = localStorage.getItem('accessToken');
    if (accessToken) {
      const decodedToken = this.getDecodedToken(accessToken);
      this.isNavbarVisible = decodedToken.role === 'USER' || decodedToken.role === 'ADMIN';
    } else {
      this.isNavbarVisible = false;
    }
  }

  logout(): void {
    localStorage.clear();
    this.router.navigate(['/login']);
  }

  private getDecodedToken(token: string): DecodedToken {
    const base64Url = token.split('.')[1];
    const base64 = base64Url.replace(/-/g, '+').replace(/_/g, '/');
    const jsonPayload = decodeURIComponent(
      atob(base64)
        .split('')
        .map((c) => '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2))
        .join('')
    );

    return JSON.parse(jsonPayload) as DecodedToken;
  }
}
