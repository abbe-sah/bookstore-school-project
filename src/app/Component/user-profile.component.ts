/**
 * Author: Abbe S
 *
 * Användarprofilen hanteras här.
 * 

*/
import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

interface User {
  username: string;
  role: string;
  purchases: Purchase[];
}

interface Purchase {
  title: string;
  author: string;
  quantity: number;
}

@Component({
  selector: 'app-user-profile',
  templateUrl: '../View/user-profile.component.html',
  styleUrls: ['../Style/user-profile.component.css']
})
export class UserProfileComponent implements OnInit {
  user: User | undefined;

  constructor(private http: HttpClient) {}

  ngOnInit(): void {
    const accessToken = localStorage.getItem('accessToken') || '';

    const headers = new HttpHeaders().set('Authorization', `Bearer ${accessToken}`);
    this.http.get<{ user: User }>('http://localhost:3000/library/profile', { headers }).subscribe(
      (data) => {
        this.user = data.user;
        console.log(this.user);
        
      },
      (error) => {
        console.error('Error geting user profile:', error);
      }
    );
  }
}
