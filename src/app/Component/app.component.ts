import { Component } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: '../View/app.component.html',
  styleUrls: ['../Style/app.component.css']
})
export class AppComponent {
  title = 'bookstore-frontend';

  constructor(private router: Router) {}

  isNavbarVisible(): boolean {
    const currentUserRole = localStorage.getItem('role');
    return currentUserRole === 'ADMIN' || currentUserRole === 'USER';
  }
  
}
