/**
 * Author: Abbe S
 *
 * Komponenten `BookListComponent` hanterar visningen av boklistan och interaktionen   * med användaren.
 * 
 * fetchBook -- hämtar boklistan från servern och uppdaterar `books`-arrayen. 
 * searchBooks-- Söker efter böcker baserat på användarens sökfråga och uppdaterar `books`-arrayen.
 * Om sökfrågan är tom hämtas hela listan.
 *
 * loadUser -- Hämtar användarprofilen från servern och uppdaterar `user`-objektet.
 *
 * orderBook -- Beställer en bok med en viss kvantitet. Uppdaterar användarens köp och * minskar bokkvantiteten.
 * Skickar även en uppdatering till servern för att spara användarens köp.
 */

import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Subscription } from 'rxjs';

interface Book {
  title: string;
  author: string;
  quantity: number;
  orderQuantity?: number;
}

interface User {
  username: string;
  role: string;
  purchases?: Purchase[];
}

interface Purchase {
  title: string;
  author: string;
  quantity: number;
}

@Component({
  selector: 'app-book-list',
  templateUrl: '../View/book-list.component.html',
  styleUrls: ['../Style/book-list.component.css']
})
export class BookListComponent implements OnInit, OnDestroy {
  books: Book[] = [];
  searchQuery = '';
  private subscription: Subscription | undefined;
  user: User | undefined;

  constructor(private http: HttpClient) {}

  ngOnInit(): void {
    this.fetchBooks();
    this.loadUser();
  }

  fetchBooks(): void {
    this.subscription = this.http
      .get<{ books: Book[] }>('http://localhost:3000/library/books')
      .subscribe(
        (data) => {
          this.books = data.books;
        },
        (error: any) => {
          console.error('Error fetching books:', error);
        }
      );
  }

  searchBooks(): void {
    if (this.searchQuery.trim() !== '') {
      const url = `http://localhost:3000/library/books/search?q=${encodeURIComponent(this.searchQuery)}`;
      this.subscription = this.http
        .get<{ books: Book[] }>(url)
        .subscribe(
          (data) => {
            this.books = data.books;
          },
          (error: any) => {
            console.error('Error searching books:', error);
          }
        );
    } else {
      this.fetchBooks();
    }
  }

  loadUser(): void {
    const accessToken = localStorage.getItem('accessToken') || '';

    const headers = {
      Authorization: `Bearer ${accessToken}`
    };

    this.http.get<{ user: User }>('http://localhost:3000/library/profile', { headers }).subscribe(
      (data) => {
        this.user = data.user;
        console.log(this.user);
      },
      (error) => {
        console.error('Error fetching user profile:', error);
      }
    );
  }

  orderBook(book: Book, quantity: number | undefined): void {
    if (quantity !== undefined && this.user) {
      if (quantity <= book.quantity && quantity > 0) {
        if (!this.user.purchases) {
          this.user.purchases = [];
        }
        this.user.purchases.push({ title: book.title, author: book.author, quantity });
  
        // calculate the qyantity
        book.quantity -= quantity;
        console.log(`Ordered ${quantity} copies of ${book.title}`);
        console.log(this.user.purchases);

        // Update the order on Jakobs server
        const accessToken = localStorage.getItem('accessToken') || '';

        const headers = {
          Authorization: `Bearer ${accessToken}`
        };

        this.http.patch<any>('http://localhost:3000/library/profile', { purchases: this.user.purchases }, { headers }).subscribe(
          () => {
            console.log('Order updated on the server');
          },
          (error) => {
            console.error('Error updating order:', error);
          }
        );
      } else {
        console.error('quantity issues');
      }
    } else {
      console.error('Invalid quantity or user data');
    }
  }
  

  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }
}
