/**
 * Author: Abbe S
 *
 * Denna komponent hanterar registreringen, om det lyckas så skickas användaren till loginsidan.
 * 

*/

import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: '../View/register.component.html',
  styleUrls: ['../Style/register.component.css']
})
export class RegisterComponent {
  username = '';
  password = '';

  constructor(private http: HttpClient, private router: Router) {}

  register(): void {
    this.http
      .post<string>('http://localhost:3000/auth/register', { username: this.username, password: this.password })
      .subscribe({
        next: () => {
          this.router.navigate(['/login']);
        },
        error: (error: string) => {
          console.error(error);
        }
      });
  }
}
