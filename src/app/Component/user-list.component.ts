/**
 * Author: Abbe S
 *
 * Denna komponent hämtar användarna från apiet ifall det är admin, annars har man inte tillgång till datan.
 * 

*/
import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Subscription } from 'rxjs';

interface User {
  username: string;
  role: string;
}

@Component({
  selector: 'app-user-list',
  templateUrl: '../View/user-list.component.html',
  styleUrls: ['../Style/user-list.component.css']
})
export class UserListComponent implements OnInit, OnDestroy {
  users: User[] = [];
  isAdmin = false;
  private subscription: Subscription | undefined;

  constructor(private http: HttpClient) {}

  ngOnInit(): void {
    const accessToken = localStorage.getItem('accessToken');
    if (accessToken) {
      const decodedToken = this.getDecodedToken(accessToken);
      this.isAdmin = decodedToken.role === 'ADMIN';
      if (this.isAdmin) {
        this.loadUsers(accessToken);
      }
    }
  }

  loadUsers(accessToken: string): void {
    const headers = new HttpHeaders().set('Authorization', `Bearer ${accessToken}`);
    this.subscription = this.http
      .get<{ users: User[] }>('http://localhost:3000/admin/users', { headers })
      .subscribe(
        (data) => {
          this.users = data.users;
        },
        (error: any) => {
          console.error('Error getting users:', error);
        }
      );
  }

  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }

  private getDecodedToken(token: string): { role: string } {
    const base64Url = token.split('.')[1];
    const base64 = base64Url.replace(/-/g, '+').replace(/_/g, '/');
    const jsonPayload = decodeURIComponent(
      atob(base64)
        .split('')
        .map((c) => '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2))
        .join('')
    );

    return JSON.parse(jsonPayload) as { role: string };
  }
}
