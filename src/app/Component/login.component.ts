/**
 * Author: Abbe S
 *
 * hanterar inloggningen och sparar datan i localstorage, skickar senare användaren till profilsidan om allt lyckas.
*/
import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

interface LoginResponse {
  accessToken: string;
  role: string;
}

@Component({
  selector: 'app-login',
  templateUrl: '../View/login.component.html',
  styleUrls: ['../Style/login.component.css']
})
export class LoginComponent {
  username = '';
  password = '';
  loginError = '';

  constructor(private http: HttpClient, private router: Router) {}

  login(): void {
    this.http
      .post<LoginResponse>('http://localhost:3000/auth/login', { username: this.username, password: this.password })
      .subscribe({
        next: (data: LoginResponse) => {
          console.log(data);
          localStorage.setItem('accessToken', data.accessToken);
          localStorage.setItem('role', data.role);
          console.log(data.role);
          this.router.navigate(['/profile']);
        },
        error: (error: any) => {
          console.error(error);
          this.loginError = 'Failed to find user with login values';
        }
      });
  }

  loginAsGuest(): void {
    localStorage.setItem('role', 'GUEST');
    this.router.navigate(['/books']);
  }
}
